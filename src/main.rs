mod schema;

use diesel::{AsChangeset, Connection, debug_query, Insertable, IntoSql, PgConnection, Queryable, QueryableByName, QueryDsl, RunQueryDsl};
use crate::schema::persons;
pub const DATABASE_URL: &str = "DATABASE_URL";

pub fn get_database_url()->String{
    dotenv::var(DATABASE_URL).unwrap()
}

pub fn establish_connection() -> PgConnection {
    let database_url = &get_database_url();
    PgConnection::establish(database_url)
        .unwrap_or_else(|_| panic!("Error connecting to {}", database_url))
}


#[derive(Queryable,Insertable, AsChangeset, QueryableByName, Clone)]
pub struct Person {
    pub id: i32,
    pub name: String,
    pub age: i32
}


impl Person{
    pub fn new(id: i32, name: String, age: i32) -> Person {
        Person {
            id,
            name,
            age
        }
    }

    pub fn insert(person_to_insert: Person, conn: &mut PgConnection) -> Result<Person,
        diesel::result::Error> {
        use crate::schema::persons::dsl::*;
        diesel::insert_into(persons)
            .values(person_to_insert)
            .get_result(conn)
    }

    pub fn working_count(conn: &mut PgConnection) -> Result<i64, diesel::result::Error> {
        use crate::schema::persons::dsl::*;
        persons.count().get_result(conn)
    }

    pub fn get_count(conn: &mut PgConnection) -> Result<i64, diesel::result::Error> {
        use crate::schema::persons::dsl::*;
        persons.order_by(age).count().get_result(conn)
    }
}


fn main() {
    if Person::working_count(&mut establish_connection()).expect("TODO: panic message") == 0 {
        Person::insert(Person::new(1, "test".to_string(), 1), &mut establish_connection()).expect
        ("TODO: panic message");
        Person::insert(Person::new(2, "test123".to_string(), 1), &mut establish_connection()).expect
        ("TODO: panic message");
    }

    Person::get_count(&mut establish_connection()).expect("TODO: panic message");

}
