// @generated automatically by Diesel CLI.

diesel::table! {
    persons (id) {
        id -> Int4,
        name -> Text,
        age -> Int4,
    }
}
